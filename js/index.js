let num1, num2, sum, prod, diff, quo;

num1 = prompt("Enter number[1]:");
num2 = prompt("Enter number[2]:");

addFunc(num1, num2);
subFunc(num1, num2);
mulFunc(num1, num2);
divFunc(num1, num2);

console.log(
	"Sum: " + sum + "\n" + 
	"Difference: " + diff + "\n" +
	"Product: " + prod + "\n" + 
	"Quotient: " + quo
);



/* functions */
function addFunc(num1, num2){
	sum = parseInt(num1) + parseInt(num2);
	return sum; 
}

function subFunc(num1, num2){
	diff = num1 - num2;
	return diff; 
}

function mulFunc(num1, num2){
	prod = num1 * num2;
	return prod; 
}

function divFunc(num1, num2){
	quo = num1 / num2;
	return quo; 
}
